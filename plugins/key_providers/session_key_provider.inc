<?php
/**
 * @file
 * The ctools encrypt key provider plugin.
 */

$plugin = array(
  'title' => t('Session key'),
  'description' => t('A temporary session key derived from the current session id.'),
  'key callback' => 'session_key_provider_get_session_key',
  'settings form' => 'session_key_provider_key_settings_form',
  'submit callback' => 'session_key_provider_key_settings_form_submit',
  'static key' => TRUE,
);

/**
 * Get default settings.
 */
function session_key_provider_get_defaults() {
  return array(
    'length' => 16,
    'salt' => '',
  );
}

/**
 * Settings form.
 */
function session_key_provider_key_settings_form($defaults) {
  $form = array();

  $defaults += session_key_provider_get_defaults();

  $form['length'] = array(
    '#title' => t('Length of the key in bytes'),
    '#type' => 'textfield',
    '#default_value' => $defaults['length'],
    '#required' => TRUE,
    '#size' => 5,
    '#field_suffix' => t('Bytes'),
    '#element_validate' => array('session_key_provider_key_settings_form_byte_validate'),
  );

  $form['salt'] = array(
    '#title' => t('Salt'),
    '#type' => 'textfield',
    '#default_value' => $defaults['salt'],
  );

  return $form;
}

/**
 * Element validator for the length settings.
 */
function session_key_provider_key_settings_form_byte_validate($element, &$form, &$form_state) {
  $length = (int) $element['#value'];
  if ($length < 0 || $length > 255 * 32) {
    form_set_error($element, t('Please enter a valid key size. The size must match the chosen encryption method and must be smaller than 8160 bytes.'));
  }
}

/**
 * Key callback.
 */
function session_key_provider_get_session_key($settings) {
  $settings += session_key_provider_get_defaults();
  $session_id = session_id();

  // Force anonymous sessions to be persisted.
  if (!$GLOBALS['user']->uid) {
    $_SESSION['session_key_provider_used'] = TRUE;
  }

  return session_key_provider_derive_key('sha256', $session_id, (int) $settings['length'], 'session_key_provider', $settings['salt'] ? $settings['salt'] : NULL);
}
